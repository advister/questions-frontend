import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { Store } from '@ngrx/store';
import { SharedModule } from 'src/app/shared/shared.module';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AbstractControl } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { TestStore } from 'src/testing/utils';
import { AuthState } from '../reducers/auth.reducer';
import { Login } from '../actions/auth.actions';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let store: TestStore<AuthState>;
  let username: AbstractControl;
  let password: AbstractControl;
  let loginButton: HTMLElement;
  let dispatchSpy;

  const validEmail = 'info@gemraad.nl';
  const validPassword = 'xxx';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, HttpClientModule, SharedModule],
      declarations: [LoginComponent],
      providers: [{ provide: Store, useClass: TestStore }]
    }).compileComponents();

    store = TestBed.get(Store);
    store.setState({ loggedIn: false, logginErrorMessage: null });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    username = component.loginForm.controls['username'];
    password = component.loginForm.controls['password'];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when the input is invalid', () => {
    username.setValue('');
    password.setValue('');

    expect(component.loginForm.invalid).toBeTruthy();
  });

  it('should have a disabled submit button with invalid input', () => {
    username.setValue('');
    password.setValue('');
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.login-button'));
    // de has the type any set to type
    // nativeElement to get intelisense
    loginButton = de.nativeElement;
    expect(loginButton.hasAttribute('disabled')).toBeTruthy();
  });

  it('should give error messages when required fields are empty', () => {
    username.setValue('');
    password.setValue('');

    expect(username.getError('required')).toBeTruthy();
    expect(password.getError('required')).toBeTruthy();
  });

  it('should give an error when username is not a valid email address', () => {
    username.setValue('xxx');
    expect(username.getError('email')).toBeTruthy();
  });

  it('should give an error when username the minimum field length of 2 is not reached', () => {
    username.setValue('x');
    const error = username.getError('minlength');
    expect(error.requiredLength).toEqual(2);
  });

  it('should be valid when the input is valid', () => {
    username.setValue(validEmail);
    password.setValue(validPassword);

    expect(component.loginForm.valid).toBeTruthy();
  });

  it('should have the submit button enabled when the input is invalid', () => {
    username.setValue(validEmail);
    password.setValue(validPassword);
    fixture.detectChanges();

    expect(component.enableSubmit()).toBeFalsy();
  });

  it('should return a logginErrorMessage from the store if it is there', () => {
    store.setState({
      loggedIn: false,
      logginErrorMessage: 'ERROR'
    });

    component.hasLogginError().subscribe(error => {
      expect(error.logginErrorMessage).toBe('ERROR');
    });
  });

  it('should submit with valid input and dispatch to the store', () => {
    username.setValue(validEmail);
    password.setValue(validPassword);
    fixture.detectChanges();

    dispatchSpy = spyOn(store, 'dispatch');

    const de = fixture.debugElement.query(By.css('.login-button'));
    loginButton = de.nativeElement;
    loginButton.click();

    expect(dispatchSpy).toHaveBeenCalledTimes(1);
    expect(dispatchSpy).toHaveBeenCalledWith(
      new Login({ username: validEmail, password: validPassword })
    );
  });
});
