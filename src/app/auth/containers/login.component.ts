import { OnInit, Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { Login, Logout } from '../actions/auth.actions';
import { hasError } from '../selectors/auth.selectors';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginError: boolean;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.email,
        Validators.minLength(2)
      ]),
      password: new FormControl(null, Validators.required)
    });

    this.loginForm.valueChanges.subscribe(() =>
      this.store.dispatch(new Logout())
    );
  }

  submitForm() {
    const f = this.loginForm.controls;
    this.store.dispatch(
      new Login({ username: f.username.value, password: f.password.value })
    );
  }

  enableSubmit() {
    return this.loginForm.invalid;
  }

  hasLogginError() {
    return this.store.select(hasError);
  }
}
