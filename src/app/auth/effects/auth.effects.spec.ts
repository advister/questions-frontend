import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AuthEffects } from './auth.effects';
import { SharedModule } from 'src/app/shared/shared.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import {
  TranslateLoader,
  TranslateModule,
  TranslateService
} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';

const TRANSLATIONS_NL = require('src/assets/i18n/nl.json');

describe('AuthEffects', () => {
  const actions$: Observable<any> = null;
  let effects: AuthEffects;
  let translate: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        AuthEffects,
        provideMockActions(() => actions$),
        TranslateService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    effects = TestBed.get(AuthEffects);
    translate = TestBed.get(TranslateService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
