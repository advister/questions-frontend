import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';

import {
  AuthActionTypes,
  Login,
  LoginFailure,
  LoginError,
  LoginSuccess
} from '../actions/auth.actions';
import { Authenticate } from '../../shared/interfaces/authenticate.interface';
import { AuthService } from '../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.Login),
    map(action => action.payload),
    exhaustMap((auth: Authenticate) => {
      this.authService.clearToken();
      return this.authService.login(auth).pipe(
        map(authToken => {
          this.authService.setToken(authToken.token);
          return new LoginSuccess();
        }),
        catchError(error => of(new LoginFailure(error)))
      );
    })
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    tap(() => this.router.navigate(['/']))
  );

  @Effect()
  loginFailure$ = this.actions$.pipe(
    ofType<LoginFailure>(AuthActionTypes.LoginFailure),
    exhaustMap(error => {
      const errorMsg = (
        'AUTH.ERRORS.' + error.payload['statusText']
      ).toUpperCase();
      return this.translate
        .get(errorMsg)
        .pipe(map(message => new LoginError({ message: message })));
    })
  );

  @Effect({ dispatch: false })
  logOut$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    tap(() => {
      this.authService.clearToken();
      this.router.navigate(['/login']);
    })
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Logout),
    tap(() => {
      this.router.navigate(['/login']);
    })
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private translate: TranslateService,
    private router: Router
  ) {}
}
