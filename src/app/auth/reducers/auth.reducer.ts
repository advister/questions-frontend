import { AuthActions, AuthActionTypes } from '../actions/auth.actions';

export interface AuthState {
  loggedIn: boolean;
  logginErrorMessage: string;
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  logginErrorMessage: null
};

export function reducer(
  Authstate = initialAuthState,
  action: AuthActions
): AuthState {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess:
      return {
        loggedIn: true,
        logginErrorMessage: null
      };

    case AuthActionTypes.Logout:
      return {
        loggedIn: false,
        logginErrorMessage: null
      };

    case AuthActionTypes.LoginError:
      return {
        loggedIn: false,
        logginErrorMessage: action.payload.message
      };

    default:
      return Authstate;
  }
}
