import { TestBed, inject } from '@angular/core/testing';

import { AuthGuard } from './auth-guard.service';
import { StoreModule, Store, combineReducers } from '@ngrx/store';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from './auth.service';
import * as fromAuth from '../reducers/auth.reducer';
import * as fromRoot from 'src/app/reducers';
import * as AuthActions from '../actions/auth.actions';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: AuthService;
  let store: Store<fromAuth.AuthState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          auth: combineReducers(fromAuth.reducer)
        }),
        HttpClientTestingModule
      ],
      providers: [AuthGuard, AuthService]
    });

    authService = TestBed.get(AuthService);
    guard = TestBed.get(AuthGuard);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', inject([AuthGuard], (service: AuthGuard) => {
    expect(service).toBeTruthy();
  }));

  it('should', () => {
    const action = new AuthActions.LoginSuccess();
    store.dispatch(action);

    store.select('loggedIn').subscribe(test => console.log(test));
    guard.canActivate().subscribe(test => console.log(test));

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
