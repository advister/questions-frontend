import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as AuthActions from '../actions/auth.actions';
import { AuthState } from '../reducers/auth.reducer';
import { isLoggedIn } from '../selectors/auth.selectors';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AuthState>, private auth: AuthService) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(isLoggedIn),
      map(authed => {
        if (!authed) {
          if (this.auth.getToken()) {
            this.store.dispatch(new AuthActions.LoginSuccess());
          } else {
            this.store.dispatch(new AuthActions.LoginRedirect());
            return false;
          }
        }

        return true;
      }),
      take(1)
    );
  }
}
