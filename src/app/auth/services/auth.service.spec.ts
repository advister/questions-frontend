import { TestBed, getTestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { Authenticate } from '../../shared/interfaces/authenticate.interface';
import { AuthToken } from '../../shared/interfaces/auth-token.interface';
import { environment } from '../../../environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

describe('AuthService', () => {
  let injector: TestBed;
  let service: AuthService;
  let httpMock: HttpTestingController;

  const mockToken: AuthToken = { token: 'TOKEN' };
  const apiEndpoint = environment.APIEndpoint + '/tokens';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });

    injector = getTestBed();
    service = injector.get(AuthService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve a token when valid authentication is provided', () => {
    const auth: Authenticate = {
      username: 'info@gemraad.nl',
      password: 'secret'
    };

    service.login(auth).subscribe((authToken: AuthToken) => {
      expect(authToken).toEqual(mockToken);
    });

    const req = httpMock.expectOne(apiEndpoint);

    expect(req.request.method).toEqual('POST');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');

    req.flush(mockToken);
  });

  it('should return an error when invalid authentication is provided', () => {
    const auth: Authenticate = {
      username: 'invalid@gemraad.nl',
      password: 'invalid'
    };

    service.login(auth).subscribe(
      response => fail('should fail with 401 Unauthorized status'),
      (error: HttpErrorResponse) => {
        const errorBody = JSON.stringify(error.error);
        expect(error).toBeTruthy();
        expect(error.status).toEqual(401);
        expect(errorBody).toContain(
          '"code":401,"message":"JWT Token not found"'
        );
      }
    );

    const req = httpMock.expectOne(apiEndpoint);

    expect(req.request.responseType).toEqual('json');

    req.flush(
      { code: 401, message: 'JWT Token not found' },
      { status: 401, statusText: 'Unauthorized' }
    );
  });

  it('should set a token in the session storage and retrieve it', () => {
    service.setToken(mockToken.token);

    const token = service.getToken();

    expect(token).toEqual(mockToken.token);
  });

  it('should clear a token', () => {
    service.setToken(mockToken.token);

    let token = service.getToken();
    expect(token).toEqual(mockToken.token);

    service.clearToken();
    token = service.getToken();
    expect(token).toBeNull();
  });
});
