import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthToken } from '../../shared/interfaces/auth-token.interface';
import { Authenticate } from '../../shared/interfaces/authenticate.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(auth: Authenticate): Observable<AuthToken> {
    const apiEndpoint = environment.APIEndpoint + '/tokens';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Basic ${btoa(auth.username + ':' + auth.password)}`
      })
    };

    return this.http.post<AuthToken>(apiEndpoint, {}, httpOptions);
  }

  setToken(token: string) {
    if (!token) {
      this.clearToken();
      return false;
    }

    return sessionStorage.setItem('token', token);
  }

  getToken() {
    return sessionStorage.getItem('token');
  }

  clearToken() {
    sessionStorage.clear();
  }
}
