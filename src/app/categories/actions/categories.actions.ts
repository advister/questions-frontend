import { Action } from '@ngrx/store';
import { Category } from '../interfaces/category.interface';
import { Categoryitem } from '../interfaces/categoryItem.interface';

export enum CategoriesActionTypes {
  CategoryRequested = '[View Category Page] Category Requested',
  CategoryLoaded = '[Category API] Category Loaded',
  CategorySaved = '[Edit Category Dialog] Category Saved',
  CategoryItemSaved = '[Edit Category Item Dialog] Category Item Saved',
  AllCategoriesRequested = '[Category List Page] All Categories Requested',
  AllCategoriesLoaded = '[Categories API] All Categories Loaded'
}

export class CategoryRequested implements Action {
  readonly type = CategoriesActionTypes.CategoryRequested;

  constructor(public payload: { categoryId: number }) {}
}

export class CategoryLoaded implements Action {
  readonly type = CategoriesActionTypes.CategoryLoaded;

  constructor(public payload: { category: Category }) {}
}

export class CategorySaved implements Action {
  readonly type = CategoriesActionTypes.CategorySaved;

  constructor(public payload: { category: Category }) {}
}

export class CategoryItemSaved implements Action {
  readonly type = CategoriesActionTypes.CategoryItemSaved;

  constructor(public payload: { category: Category }) {}
}

export class AllCategoriesRequested implements Action {
  readonly type = CategoriesActionTypes.AllCategoriesRequested;
}

export class AllCategoriesLoaded implements Action {
  readonly type = CategoriesActionTypes.AllCategoriesLoaded;

  constructor(public payload: { categories: Category[] }) {}
}

export type CategoriesActions =
  | CategoryRequested
  | CategoryLoaded
  | CategorySaved
  | CategoryItemSaved
  | AllCategoriesRequested
  | AllCategoriesLoaded;
