import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './containers/categories.component';
import { AuthGuard } from '../auth/services/auth-guard.service';
import { CategoryComponent } from './containers/category.component';
import { CategoryResolver } from './services/category.resolver';
import { CategoriesResolver } from './services/categories.resolver';

const routes: Routes = [
  {
    path: ':id',
    canActivate: [AuthGuard],
    component: CategoryComponent,
    resolve: { category: CategoryResolver }
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: CategoriesComponent,
    resolve: { categories: CategoriesResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule {}
