import { NgModule } from '@angular/core';
import { CategoriesRoutingModule } from './categories-routing.module';
import { StoreModule } from '@ngrx/store';
import * as fromCategories from './reducers/categories.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CategoriesEffects } from './effects/categories.effects';
import { SharedModule } from '../shared/shared.module';
import { CategoriesComponent } from './containers/categories.component';
import { CategoryComponent } from './containers/category.component';
import { CategoryDataTableComponent } from './components/category-data-table/category-data-table.component';
import { CategoryDialogComponent } from './components/category-dialog/category-dialog.component';
import { CategoryItemsDialogComponent } from './components/category-items-dialog/category-items-dialog.component';
import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule
} from '@angular/material';
import { CategoryItemsDataTableComponent } from './components/category-items-data-table/category-items-data-table.component';

@NgModule({
  imports: [
    SharedModule,
    CategoriesRoutingModule,
    StoreModule.forFeature('categories', fromCategories.reducer),
    EffectsModule.forFeature([CategoriesEffects]),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  declarations: [
    CategoriesComponent,
    CategoryComponent,
    CategoryDataTableComponent,
    CategoryDialogComponent,
    CategoryItemsDialogComponent,
    CategoryItemsDataTableComponent
  ],
  entryComponents: [CategoryDialogComponent, CategoryItemsDialogComponent]
})
export class CategoriesModule {}
