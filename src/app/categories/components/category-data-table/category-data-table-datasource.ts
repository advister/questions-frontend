import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map, tap } from 'rxjs/operators';
import { Observable, merge, from } from 'rxjs';
import { Category } from '../../interfaces/category.interface';

/**
 * Data source for the CategoryDataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CategoryDataTableDataSource implements DataSource<Category> {
  data: Category[];

  constructor(private paginator: MatPaginator, private sort: MatSort) {}

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Category[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.

    const dataMutations = [
      this.data,
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    // Show an empty page when there
    // are no rows
    if (this.paginator.length === 0) {
      return from([this.data]);
    }

    return merge(...dataMutations).pipe(
      map(() => {
        return this.getPagedData(this.getSortedData([...this.data]));
      })
    );
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: Category[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: Category[]) {
    if (this.sort.active !== undefined) {
      if (this.sort.active === 'id' || this.sort.active === 'visible') {
        return data.sort((a, b) =>
          compare(
            +a[this.sort.active],
            +b[this.sort.active],
            this.sort.direction
          )
        );
      }
      if (this.sort.direction === 'asc') {
        return data.sort((a, b) =>
          a[this.sort.active].localeCompare(b[this.sort.active])
        );
      } else {
        data.sort((a, b) =>
          a[this.sort.active].localeCompare(b[this.sort.active])
        );
        return data.reverse();
      }
    } else {
      return data;
    }
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc === 'asc' ? 1 : -1);
}
