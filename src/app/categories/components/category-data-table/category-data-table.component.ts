import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  AfterViewInit,
  OnChanges
} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { CategoryDataTableDataSource } from './category-data-table-datasource';
import { Category } from '../../interfaces/category.interface';
import { Observable, fromEvent, of } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CategoryDialogComponent } from '../category-dialog/category-dialog.component';
import { Router } from '@angular/router';
import { map, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-category-data-table',
  templateUrl: './category-data-table.component.html',
  styleUrls: ['./category-data-table.component.scss']
})
export class CategoryDataTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  searchForm: FormGroup;

  categoryDataSource: CategoryDataTableDataSource;

  @Input()
  categories$: Observable<Category[]>;

  @Output()
  saveCategory$: EventEmitter<Category> = new EventEmitter();

  @Output()
  filter: EventEmitter<string> = new EventEmitter();

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'description', 'visible', 'items'];

  constructor(private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.searchForm = new FormGroup({
      searchInput: new FormControl(null),
      selectVisibility: new FormControl(true)
    });

    this.filter.emit(this.searchForm.value);

    this.searchForm.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        tap(searchEvent => {
          this.filter.emit(searchEvent);
        })
      )
      .subscribe();
  }

  ngOnChanges() {
    this.categories$.subscribe(categories => {
      this.categoryDataSource = new CategoryDataTableDataSource(
        this.paginator,
        this.sort
      );
      this.categoryDataSource.data = categories;
    });
  }

  showBoolean(bool: boolean): string {
    return bool === true ? 'visibility' : 'visibility_off';
  }

  upsertCategory(category: Category = null): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';

    dialogConfig.data = {};
    if (category) {
      dialogConfig.data = category;
    }
    const dialogRef = this.dialog.open(CategoryDialogComponent, dialogConfig);

    dialogRef.componentInstance.saveCategory$.subscribe(newCategory => {
      this.saveCategory$.emit(newCategory);
      if (!newCategory.id) {
        this.paginator.lastPage();
      }
    });
  }

  updateItems(event, category: Category): void {
    event.stopPropagation();

    this.router.navigate(['/categories', +category.id]);
  }
}
