import { Component, Inject, OnInit, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Category } from '../../interfaces/category.interface';

@Component({
  selector: 'app-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {
  form: FormGroup;

  @Output()
  saveCategory$: EventEmitter<Category> = new EventEmitter();

  constructor(
    private dialogRef: MatDialogRef<CategoryDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private category: Category
  ) {}

  ngOnInit() {
    if (this.category.visible === undefined) {
      this.category.visible = true;
    }

    this.form = this.fb.group({
      id: [this.category.id, []],
      description: [this.category.description, Validators.required],
      name: [this.category.name, Validators.required],
      visible: [this.category.visible, []]
    });
  }

  enableSave() {
    return this.form.invalid;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    const category = this.form.value;

    this.saveCategory$.emit(category);

    this.dialogRef.close();
  }
}
