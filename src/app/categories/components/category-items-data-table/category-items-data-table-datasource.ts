import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge, empty, from } from 'rxjs';
import { Categoryitem } from '../../interfaces/categoryItem.interface';
import { UtilityService } from 'src/app/shared/services/utility.service';
import { Category } from '../../interfaces/category.interface';

/**
 * Data source for the CategoryItemsDataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CategoryItemsDataTableDataSource
  implements DataSource<Categoryitem> {
  data: Categoryitem[];

  constructor(private paginator: MatPaginator, private sort: MatSort) {}

  loadData(categoryItems: Categoryitem[], categoryId: number, period: string) {
    const load = [];
    let item = null;

    this.sort.active = 'sequence';
    this.sort.direction = 'asc';
    const sortedItems = this.getSortedData([...categoryItems]);

    sortedItems.forEach(element => {
      item = { ...element, category: categoryId };
      load.push(item);
    });
    this.data = load;
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Categoryitem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      this.data,
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    // Show an empty page when there
    // are no rows
    if (this.paginator.length === 0) {
      return from([this.data]);
    }

    return merge(...dataMutations).pipe(
      map(test => {
        return this.getPagedData([...this.data]);
      })
    );
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: Categoryitem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: Categoryitem[]) {
    return data.sort((a, b) =>
      compare(+a[this.sort.active], +b[this.sort.active], this.sort.direction)
    );
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc === 'asc' ? 1 : -1);
}
