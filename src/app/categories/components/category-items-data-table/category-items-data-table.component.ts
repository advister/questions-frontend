import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import {
  MatPaginator,
  MatSort,
  MatDialogConfig,
  MatDialog
} from '@angular/material';
import { CategoryItemsDataTableDataSource } from './category-items-data-table-datasource';
import { Category } from '../../interfaces/category.interface';
import { CategoryItemsDialogComponent } from '../category-items-dialog/category-items-dialog.component';
import { Categoryitem } from '../../interfaces/categoryItem.interface';
import { Observable, empty } from 'rxjs';
import { concat, tap } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-category-items-data-table',
  templateUrl: './category-items-data-table.component.html',
  styleUrls: ['./category-items-data-table.component.scss']
})
export class CategoryItemsDataTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  categoryitemsDataSource: CategoryItemsDataTableDataSource;

  @Input()
  category: Category;

  @Input()
  categoryItems$: Observable<Categoryitem[]>;

  @Output()
  saveCategoryitem$: EventEmitter<Categoryitem> = new EventEmitter();

  @Output()
  selectPeriod: EventEmitter<string> = new EventEmitter();

  selectedPeriod: string;

  selectForm: FormGroup;
  options: any[];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'id',
    'category',
    'sequence',
    'code',
    'name',
    'startdate',
    'enddate',
    'up',
    'down'
  ];

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    const defaultPeriod = 'PRESENT';

    this.selectForm = new FormGroup({
      selectPeriod: new FormControl(defaultPeriod)
    });

    this.options = [
      { value: 'PRESENT', viewValue: 'PERIOD.PRESENT' },
      { value: 'ALL', viewValue: 'PERIOD.ALL' },
      { value: 'FUTURE', viewValue: 'PERIOD.FUTURE' },
      { value: 'PAST', viewValue: 'PERIOD.PAST' }
    ];

    this.selectPeriod.emit(this.selectForm.value.selectPeriod);

    this.selectForm.valueChanges.subscribe(selection => {
      this.selectedPeriod = selection.selectPeriod;
      this.selectPeriod.emit(selection.selectPeriod);
    });
  }

  ngOnChanges() {
    this.categoryItems$.subscribe(categoryItems => {
      this.categoryitemsDataSource = new CategoryItemsDataTableDataSource(
        this.paginator,
        this.sort
      );

      this.categoryitemsDataSource.loadData(
        categoryItems,
        this.category.id,
        this.selectedPeriod
      );
    });
  }

  up(event, item, data) {
    event.stopPropagation();

    const idx = data.indexOf(item);

    if (idx > 0) {
      const itemDown = data[idx - 1];
      const sequenceSave = item.sequence;
      item.sequence = itemDown.sequence;
      itemDown.sequence = sequenceSave;

      this.saveCategoryitem$.emit(item);
      this.saveCategoryitem$.emit(itemDown);
    }
  }

  down(event, item, data) {
    event.stopPropagation();

    const idx = data.indexOf(item);

    if (idx < data.length - 1) {
      const itemUp = data[idx + 1];
      const sequenceSave = item.sequence;
      item.sequence = itemUp.sequence;
      itemUp.sequence = sequenceSave;

      this.saveCategoryitem$.emit(item);
      this.saveCategoryitem$.emit(itemUp);
    }
  }

  upsertCategoryItems(categoryItem: Categoryitem = null): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';

    dialogConfig.data = {};
    if (categoryItem) {
      dialogConfig.data = categoryItem;
    }
    const dialogRef = this.dialog.open(
      CategoryItemsDialogComponent,
      dialogConfig
    );

    dialogRef.componentInstance.saveCategoryitem$.subscribe(newCategoryitem => {
      newCategoryitem.category = this.category.id;
      this.saveCategoryitem$.emit(newCategoryitem);
      if (!newCategoryitem.id) {
        this.paginator.lastPage();
      }
    });
  }
}
