import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Categoryitem } from '../../interfaces/categoryItem.interface';
import { getLocaleDayNames } from '@angular/common';

@Component({
  selector: 'app-category-items-dialog',
  templateUrl: './category-items-dialog.component.html',
  styleUrls: ['./category-items-dialog.component.scss']
})
export class CategoryItemsDialogComponent implements OnInit {
  form: FormGroup;

  @Output()
  saveCategoryitem$: EventEmitter<Categoryitem> = new EventEmitter();

  constructor(
    private dialogRef: MatDialogRef<CategoryItemsDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private categoryitem: Categoryitem
  ) {}

  ngOnInit() {
    let startdate: string;
    let enddate: string;

    if (this.categoryitem.startdate) {
      const start = this.categoryitem.startdate.split('-');

      startdate =
        start[2].substr(0, 4) +
        '-' +
        ('0' + start[1]).substr(-2) +
        '-' +
        ('0' + start[0]).substr(-2);
    }

    if (this.categoryitem.enddate) {
      const end = this.categoryitem.enddate.split('-');

      enddate =
        end[2].substr(0, 4) +
        '-' +
        ('0' + end[1]).substr(-2) +
        '-' +
        ('0' + end[0]).substr(-2);
    }

    this.form = this.fb.group({
      id: [this.categoryitem.id, []],
      sequence: [this.categoryitem.sequence, []],
      category: [this.categoryitem.category, []],
      code: [this.categoryitem.code, Validators.required],
      name: [this.categoryitem.name, Validators.required],
      startdate: [startdate, Validators.required],
      enddate: [enddate, []]
    });
  }

  enableSave() {
    return this.form.invalid;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    const categoryitem = this.form.value;

    let date = new Date(categoryitem.startdate);
    categoryitem.startdate = date.toLocaleString('nl-NL');

    categoryitem.startdate = categoryitem.startdate.substr(
      0,
      categoryitem.startdate.length - 8
    );

    date = new Date(categoryitem.enddate);
    categoryitem.enddate = date.toLocaleString('nl-NL');

    categoryitem.enddate = categoryitem.enddate.substr(
      0,
      categoryitem.enddate.length - 8
    );

    this.saveCategoryitem$.emit(categoryitem);

    this.dialogRef.close();
  }
}
