import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { selectAllCategories } from '../selectors/categories.selectors';
import { Category } from '../interfaces/category.interface';
import { Observable } from 'rxjs';
import { CategoriesService } from '../services/categories.service';
import { AppState } from 'src/app/reducers';
import { CategorySaved } from '../actions/categories.actions';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})

//
// This component is the container of the
// categories screen.
//
// The datatable in the categories screen
// is handeled by the sources in the
// categories/components.category-data-table
// directory.
//
// The (saveCategory$) and (filter)
// events are emitted by the CategoryDataTableComponent.
//
// [categories$] is the input for the
// CategoryDataTableComponent. Filtering of
// the categories is done in this component
// where a filter event is triggered in the
// datatable.
//
export class CategoriesComponent implements OnInit {
  categories$: Observable<Category[]>;
  category: Category;

  constructor(
    private store$: Store<AppState>,
    private categoriesService: CategoriesService
  ) {}

  ngOnInit() {
    this.categories$ = this.store$.pipe(select(selectAllCategories()));
  }

  //
  // Filter the categories with the
  // data of a filter event which is
  // triggered in the datatable
  //
  filterCategory(filter: any) {
    this.categories$ = this.store$.pipe(
      select(selectAllCategories()),
      map(categories => {
        // Always filter on visibility
        categories = categories.filter(
          category => category.visible === filter.selectVisibility
        );
        // If there is a search string
        // then filter on this too.
        if (filter.searchInput) {
          const result = categories.filter(
            category =>
              category.name
                .toLowerCase()
                .indexOf(filter.searchInput.toLowerCase()) !== -1 ||
              category.description
                .toLowerCase()
                .indexOf(filter.searchInput.toLowerCase()) !== -1
          );

          return result;
        }
        return categories;
      })
    );
  }

  //
  // Save the category
  // Todo put it in effects ?
  //
  saveCategory(category: Category) {
    this.categoriesService
      .saveCategory(category)
      .subscribe(result =>
        this.store$.dispatch(new CategorySaved({ category: result }))
      );
  }
}
