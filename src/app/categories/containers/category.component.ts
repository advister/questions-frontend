import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Category } from '../interfaces/category.interface';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { Categoryitem } from '../interfaces/categoryItem.interface';
import { CategoryItemService } from '../services/category-item.service';
import { CategoryItemSaved } from '../actions/categories.actions';
import { Observable, from } from 'rxjs';
import { selectCategoryById } from '../selectors/categories.selectors';
import { map, tap, merge, filter } from 'rxjs/operators';
import { UtilityService } from 'src/app/shared/services/utility.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  data: any;
  category: Category;
  categoryItems$: Observable<Categoryitem[]>;
  category$: Observable<Category>;
  sequence$: any;
  categoryId: number;

  constructor(
    private route: ActivatedRoute,
    private store$: Store<AppState>,
    private categoryItemService: CategoryItemService,
    private utilityService: UtilityService
  ) {}

  ngOnInit() {
    this.categoryId = +this.route.snapshot.paramMap.get('id');

    this.category$ = this.store$.pipe(
      select(selectCategoryById(this.categoryId))
    );

    this.category$.subscribe(category => (this.category = category));

    this.categoryItems$ = this.store$.pipe(
      select(selectCategoryById(this.categoryId)),
      map(category => category.categoryitems)
    );
  }

  selectPeriod(selectedPeriod: string) {
    this.categoryItems$ = this.store$.pipe(
      select(selectCategoryById(this.categoryId)),
      map(category => category.categoryitems),
      map(
        categoryitems =>
          (categoryitems = categoryitems.filter(categoryitem =>
            this.utilityService.selectPeriod(categoryitem, selectedPeriod)
          ))
      )
    );
  }

  saveCategoryitem(categoryitem: Categoryitem) {
    const items = [];
    let newItem = true;

    this.categoryItemService
      .saveCategoryItem(categoryitem)
      .pipe(
        tap(result => {
          this.category.categoryitems.forEach(element => {
            if (element.id === result.id) {
              newItem = false;
              element = { ...element, ...result };
            }
            items.push(element);
          });
          if (newItem) {
            items.push(result);
          }

          const newCategory = {
            ...this.category,
            categoryitems: items
          };
          return this.store$.dispatch(
            new CategoryItemSaved({
              category: newCategory
            })
          );
        })
      )
      .subscribe();
  }
}
