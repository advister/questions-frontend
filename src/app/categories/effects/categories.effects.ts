import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import {
  CategoriesActionTypes,
  CategoryRequested,
  CategoryLoaded,
  AllCategoriesRequested,
  AllCategoriesLoaded
} from '../actions/categories.actions';
import {
  tap,
  mergeMap,
  map,
  catchError,
  withLatestFrom,
  filter
} from 'rxjs/operators';
import { CategoriesService } from '../services/categories.service';
import { of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { allCategoriesLoadedSelector } from '../selectors/categories.selectors';

@Injectable()
export class CategoriesEffects {
  @Effect()
  loadAllCategories$ = this.actions$.pipe(
    ofType<AllCategoriesRequested>(
      CategoriesActionTypes.AllCategoriesRequested
    ),
    withLatestFrom(this.store$.pipe(select(allCategoriesLoadedSelector))),
    filter(([action, allCategoriesLoaded]) => !allCategoriesLoaded),
    mergeMap(() => this.categoriesService.findAll()),
    map(categories => new AllCategoriesLoaded({ categories }))
  );

  @Effect()
  loadCategory$ = this.actions$.pipe(
    ofType<CategoryRequested>(CategoriesActionTypes.CategoryRequested),
    mergeMap(action =>
      this.categoriesService.findCategoryById(action.payload.categoryId)
    ),
    map(category => new CategoryLoaded({ category }))
  );

  constructor(
    private actions$: Actions,
    private categoriesService: CategoriesService,
    private store$: Store<AppState>
  ) {}
}
