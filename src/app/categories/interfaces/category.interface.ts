import { Categoryitem } from './categoryItem.interface';

export interface Category {
  id: number;
  name: string;
  description: string;
  visible: boolean;
  categoryitems: Categoryitem[];
}
