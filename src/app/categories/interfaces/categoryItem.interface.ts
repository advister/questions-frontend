export interface Categoryitem {
  id: number;
  code: string;
  name: string;
  sequence: number;
  startdate: string;
  enddate: string;
  category: number;
}
