import { reducer, initialCategoriesState } from './categories.reducer';

describe('Categories Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialCategoriesState, action);

      expect(result).toBe(initialCategoriesState);
    });
  });
});
