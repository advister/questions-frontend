import { Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import {
  CategoriesActions,
  CategoriesActionTypes
} from '../actions/categories.actions';
import { Category } from '../interfaces/category.interface';

export interface CategoriesState extends EntityState<Category> {
  allCategoriesLoaded: boolean;
}

export const adapter: EntityAdapter<Category> = createEntityAdapter<Category>();

export const initialCategoriesState: CategoriesState = adapter.getInitialState({
  allCategoriesLoaded: false
});

export function reducer(
  state = initialCategoriesState,
  action: CategoriesActions
): CategoriesState {
  switch (action.type) {
    case CategoriesActionTypes.AllCategoriesLoaded:
      return adapter.addAll(action.payload.categories, {
        ...state,
        allCategoriesLoaded: true
      });

    case CategoriesActionTypes.CategorySaved:
      return adapter.upsertOne(action.payload.category, state);

    case CategoriesActionTypes.CategoryItemSaved:
      return adapter.upsertOne(action.payload.category, state);

    case CategoriesActionTypes.CategoryLoaded:
      return adapter.addOne(action.payload.category, state);

    default:
      return state;
  }
}

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
