import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CategoriesState } from '../reducers/categories.reducer';
import * as fromCategory from '../reducers/categories.reducer';

export const selectCategoriesState = createFeatureSelector<CategoriesState>(
  'categories'
);

export const selectAllCategories = () =>
  createSelector(selectCategoriesState, fromCategory.selectAll);

export const selectCategoryById = (categoryId: number) =>
  createSelector(
    selectCategoriesState,
    categoriesState => categoriesState.entities[categoryId]
  );

export const allCategoriesLoadedSelector = createSelector(
  selectCategoriesState,
  categoriesState => categoriesState.allCategoriesLoaded
);
