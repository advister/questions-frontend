import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { Category } from '../interfaces/category.interface';
import { selectAllCategories } from '../selectors/categories.selectors';
import { tap, filter, first } from 'rxjs/operators';
import { AllCategoriesRequested } from '../actions/categories.actions';

@Injectable({
  providedIn: 'root'
})
export class CategoriesResolver implements Resolve<Category[]> {
  constructor(private store$: Store<AppState>) {}
  resolve(): Observable<Category[]> {
    this.store$.dispatch(new AllCategoriesRequested());
    return this.store$.pipe(
      select(selectAllCategories()),
      first()
    );
  }
}
