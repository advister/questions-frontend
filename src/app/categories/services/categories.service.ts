import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Category } from '../interfaces/category.interface';
import { Categoryitem } from '../interfaces/categoryItem.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  selectableType$: Observable<any>;

  apiEndpoint = environment.APIEndpoint + '/categories';

  constructor(private http: HttpClient) {}

  findAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.apiEndpoint, {});
  }

  findCategoryById(categoryId: number): Observable<Category> {
    return this.http.get<Category>(`${this.apiEndpoint}/${categoryId}`);
  }

  saveCategory(category: Category) {
    if (category.id) {
      return this.http.put<Category>(
        `${this.apiEndpoint}/${category.id}`,
        category
      );
    } else {
      return this.http.post<Category>(this.apiEndpoint, category);
    }
  }
}
