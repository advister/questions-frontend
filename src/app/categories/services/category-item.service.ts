import { Injectable } from '@angular/core';
import { Categoryitem } from '../interfaces/categoryItem.interface';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryItemService {
  apiEndpoint = environment.APIEndpoint + '/categoryitems';

  constructor(private http: HttpClient) {}

  saveCategoryItem(categoryitem: Categoryitem) {
    if (categoryitem.id) {
      return this.http.put<Categoryitem>(
        `${this.apiEndpoint}/${categoryitem.id}`,
        categoryitem
      );
    } else {
      return this.http.post<Categoryitem>(this.apiEndpoint, categoryitem);
    }
  }
}
