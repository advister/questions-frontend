import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { Category } from '../interfaces/category.interface';
import { selectCategoryById } from '../selectors/categories.selectors';
import { tap, filter, first } from 'rxjs/operators';
import { CategoryRequested } from '../actions/categories.actions';

@Injectable({
  providedIn: 'root'
})
export class CategoryResolver implements Resolve<Category> {
  constructor(private store$: Store<AppState>) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Category> {
    const categoryId = route.params['id'];

    return this.store$.pipe(
      select(selectCategoryById(categoryId)),
      tap(category => {
        if (!category) {
          this.store$.dispatch(new CategoryRequested({ categoryId }));
        }
      }),
      filter(category => !!category),
      first()
    );
  }
}
