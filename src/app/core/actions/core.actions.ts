import { Action } from '@ngrx/store';

export enum CoreActionTypes {
  OpenSidenav   = '[Core] Open sidenav',
  CloseSidenav  = '[Core] Close sidenav',
}

export class OpenSidenav implements Action {
  readonly type = CoreActionTypes.OpenSidenav;
}

export class CloseSidenav implements Action {
  readonly type = CoreActionTypes.CloseSidenav;
}

export type CoreActions = OpenSidenav | CloseSidenav;
