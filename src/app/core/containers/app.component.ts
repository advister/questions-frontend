import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../reducers';
import { getShowSidenav } from '../selectors/core.selectors';
import { isLoggedIn } from '../../auth/selectors/auth.selectors';
import * as CoreActions from '../actions/core.actions';
import * as AuthActions from '../../auth/actions/auth.actions';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  sidenavOpen$: Observable<boolean>;
  loggedIn$: Observable<boolean>;

  constructor(
    private store: Store<AppState>,
    private translate: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.translate.setDefaultLang('nl');
    this.translate.use('nl');

    this.sidenavOpen$ = this.store.pipe(select(getShowSidenav));
    this.loggedIn$ = this.store.pipe(select(isLoggedIn));
  }

  openSidenav() {
    this.store.dispatch(new CoreActions.OpenSidenav());
  }

  closeSidenav() {
    this.store.dispatch(new CoreActions.CloseSidenav());
  }

  openSettings() {
    this.router.navigate(['/settings']);
  }

  logout() {
    this.closeSidenav();
    this.store.dispatch(new AuthActions.Logout());
  }
}
