import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { CategoriesModule } from '../categories/categories.module';

const routes: Routes = [
  { path: '', redirectTo: '/cycles', pathMatch: 'full' },
  { path: 'login', redirectTo: '/login' },
  {
    path: 'categories',
    loadChildren: '../categories/categories.module#CategoriesModule'
  },
  { path: 'settings', redirectTo: '/settings' },
  { path: 'question-types', redirectTo: '/question-types' },
  { path: 'not-found', component: NotFoundPageComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
