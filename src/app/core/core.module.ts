import { NgModule } from '@angular/core';

import { CoreRoutingModule } from './core-routing.module';
import { AppComponent } from './containers/app.component';
import { StoreModule } from '@ngrx/store';
import * as fromCore from './reducers/core.reducer';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { LayoutComponent } from './components/layout.component';
import { SidenavComponent } from './components/sidenav.component';
import { ToolbarComponent } from './components/toolbar.component';
import { NavItemComponent } from './components/nav-item.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    CoreRoutingModule,
    StoreModule.forFeature('core', fromCore.reducer)
  ],
  declarations: [
    AppComponent,
    NotFoundPageComponent,
    LayoutComponent,
    SidenavComponent,
    ToolbarComponent,
    NavItemComponent
  ]
})
export class CoreModule {}
