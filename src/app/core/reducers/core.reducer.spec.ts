import { reducer, initialCoreState } from './core.reducer';

describe('Core Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialCoreState, action);

      expect(result).toBe(initialCoreState);
    });
  });
});
