import { CoreActions, CoreActionTypes } from '../actions/core.actions';

export interface CoreState {
  sidenavOpen: boolean;
  defaulTheme: boolean;
}

export const initialCoreState: CoreState = {
  sidenavOpen: false,
  defaulTheme: true
};

export function reducer(Corestate = initialCoreState, action: CoreActions): CoreState {
  switch (action.type) {
    case CoreActionTypes.OpenSidenav:
      return {
        ...Corestate,
        sidenavOpen: true
      };

    case CoreActionTypes.CloseSidenav:
      return {
        ...Corestate,
        sidenavOpen: false
      };

    default:
      return Corestate;
  }
}
