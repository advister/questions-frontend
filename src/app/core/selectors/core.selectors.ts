import { createSelector } from '@ngrx/store';

export const selectCoreState = state => state.core;

export const getShowSidenav = createSelector(selectCoreState, core => core.sidenavOpen);
