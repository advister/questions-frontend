import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CyclesComponent } from './containers/cycles.component';
import { AuthGuard } from '../auth/services/auth-guard.service';

const routes: Routes = [
  { path: 'cycles', canActivate: [AuthGuard], component: CyclesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CyclesRoutingModule {}
