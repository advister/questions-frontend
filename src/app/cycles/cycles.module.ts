import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CyclesRoutingModule } from './cycles-routing.module';
import { CyclesComponent } from './containers/cycles.component';

@NgModule({
  imports: [CommonModule, CyclesRoutingModule],
  declarations: [CyclesComponent]
})
export class CyclesModule {}
