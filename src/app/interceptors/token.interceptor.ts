import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';
import { Store, select } from '@ngrx/store';
import { AppState } from '../reducers';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService,
              private store$: Store<AppState>) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.auth.getToken();

    if (token) {

      const cloned = request.clone({
        headers: request.headers.set('Authorization',
            'Bearer ' + token)
      });
      return next.handle(cloned);
    } else {

      return next.handle(request);
    }
  }
}

