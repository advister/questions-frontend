import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/services/auth-guard.service';
import { QuestionTypesComponent } from './containers/question-types.component';

const routes: Routes = [
  {
    path: 'question-types',
    canActivate: [AuthGuard],
    component: QuestionTypesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionTypesRoutingModule {}
