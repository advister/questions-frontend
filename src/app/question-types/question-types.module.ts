import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionTypesRoutingModule } from './question-types-routing.module';
import { QuestionTypesComponent } from './containers/question-types.component';

@NgModule({
  imports: [
    CommonModule,
    QuestionTypesRoutingModule
  ],
  declarations: [QuestionTypesComponent]
})
export class QuestionTypesModule { }
