import { Action } from '@ngrx/store';
import { Settings } from '../interfaces/settings.interface';

export enum SettingsActionTypes {
  SettingsRequested = '[Settings Page] SetttingsRequested',
  SettingsLoaded = '[Settings API] SetttingsRequested',
  SettingsSaved = '[Settings Edit] SetttingsSaved'
}

export class SettingsRequested implements Action {
  readonly type = SettingsActionTypes.SettingsRequested;
}

export class SettingsLoaded implements Action {
  readonly type = SettingsActionTypes.SettingsLoaded;

  constructor(public payload: { settings: Settings }) {}
}

export class SettingsSaved implements Action {
  readonly type = SettingsActionTypes.SettingsSaved;

  constructor(public payload: { settings: Settings }) {}
}

export type SettingsActions =
  | SettingsRequested
  | SettingsLoaded
  | SettingsSaved;
