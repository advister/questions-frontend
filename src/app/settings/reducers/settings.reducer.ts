import { Action } from '@ngrx/store';

export interface SettingsState {
  theme: string;
  rows: number;
  language: string;
}

export const initialState: SettingsState = {
  theme: 'default',
  rows: 10,
  language: 'nl'
};

export function reducer(state = initialState, action: Action): SettingsState {
  switch (action.type) {
    default:
      return state;
  }
}
