import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './containers/settings/settings.component';
import { EffectsModule } from '@ngrx/effects';
import { SettingsEffects } from './effects/settings.effects';
import { StoreModule } from '@ngrx/store';
import * as fromSettings from './reducers/settings.reducer';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    EffectsModule.forFeature([SettingsEffects]),
    StoreModule.forFeature('settings', fromSettings.reducer)
  ],
  declarations: [SettingsComponent]
})
export class SettingsModule { }
