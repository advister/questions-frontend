import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-field-errors',
  templateUrl: './field-errors.component.html',
  styleUrls: ['./field-errors.component.css']
})
export class FieldErrorsComponent implements OnInit {
  @Input()
  field: FormControl;

  formGroup: any;
  fieldName: string;

  constructor() {}

  ngOnInit() {
    const f = this.field;
    this.formGroup = f.parent.controls;
    this.fieldName =
      Object.keys(this.formGroup).find(name => f === this.formGroup[name]) ||
      null;
  }

  getFieldErrorMessages() {
    const field = this.field;

    if (field.status === 'INVALID' && field.touched === true) {
      if (field.errors) {
        if (field.errors.required) {
          return ['ERRORS.REQUIRED'];
        }
        if (field.errors.email) {
          return ['ERRORS.EMAIL'];
        }
        if (field.errors.minlength) {
          return ['ERRORS.MINLENGTH'];
        }
      }
    }

    return [];
  }
}
