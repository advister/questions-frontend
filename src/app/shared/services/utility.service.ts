import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  selStartDate: Date;
  selEndDate: Date;
  now: Date = new Date();

  constructor() {}

  selectPeriod(row: any, period: String) {
    this.selStartDate = this.stringToDate(row.startdate);
    this.selEndDate = this.stringToDate(row.enddate);

    let rowOK = false;

    switch (period) {
      case 'PRESENT': {
        if (this.selStartDate <= this.now && this.selEndDate > this.now) {
          rowOK = true;
        }
        break;
      }
      case 'ALL': {
        rowOK = true;
        break;
      }
      case 'FUTURE': {
        if (this.selStartDate > this.now && this.selEndDate > this.now) {
          rowOK = true;
        }
        break;
      }
      case 'PAST': {
        if (this.selStartDate < this.now && this.selEndDate < this.now) {
          rowOK = true;
        }
        break;
      }
    }

    return rowOK;
  }

  stringToDate(strDate: string) {
    const date = strDate.split('-');
    return new Date(date[2] + '-' + date[1] + '-' + date[0]);
  }
}
