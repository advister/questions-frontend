import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldErrorsComponent } from './components/field-errors.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  declarations: [FieldErrorsComponent],
  exports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule,
    FieldErrorsComponent
  ]
})
export class SharedModule {}
